package com.example.musicplayer10;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

public class GetMusicTask extends AsyncTask<String, Void, String> {


    protected String doInBackground(String... details) {
        String temp="";
        MusicHTTPSClient client=new MusicHTTPSClient();

        String resJSON=client.getMusicData(details[0],details[1]);
        JSONObject jobj=null;
        try {
            jobj=new JSONObject(resJSON);

            JSONObject mainOB=jobj.getJSONObject("result");
            JSONObject track = mainOB.getJSONObject("track");
            temp=track.getString("text");
        }catch (JSONException e)
        {
            e.printStackTrace();
        }
        return temp;
    }
}

