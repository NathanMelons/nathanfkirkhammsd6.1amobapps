package com.example.musicplayer10;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.concurrent.ExecutionException;

public class Activity5 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_5);
    }

    public void getMusic(View view)
    {
        EditText artist=findViewById(R.id.editText);//artist
        EditText song=findViewById(R.id.editText2);//song
        //String BASE_URL="https://orion.apiseeds.com/api/music/lyric/";
        //String API_KEY="?apikey=MhabkleQd4CIVIna31LAptuqizBC3BVYc8FKaoBBud2WZv4xZ2sm2n8oRQMwmfjU";
        //String FINAL_URL=BASE_URL+artist+"/"+song+API_KEY;
        //TextView tx_temp1=findViewById(R.id.tx_temp);

        GetMusicTask task=new GetMusicTask();
        try{
            String[] details = {artist.getText().toString(), song.getText().toString()};
            String lyrics = task.execute(details).get();

            TextView tx_temp1=findViewById(R.id.tx_temp);
            tx_temp1.setText(String.valueOf(lyrics));

        }catch (ExecutionException e){
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
