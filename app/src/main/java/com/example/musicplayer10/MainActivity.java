package com.example.musicplayer10;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.nightonke.blurlockview.BlurLockView;
import com.nightonke.blurlockview.Directions.HideType;
import com.nightonke.blurlockview.Directions.ShowType;
import com.nightonke.blurlockview.Eases.EaseType;
import com.nightonke.blurlockview.Password;

public class MainActivity extends AppCompatActivity {
    private BlurLockView blurLockView;
    private ImageView imageView;
    final int LOGIN_ACTIVITY=5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        blurLockView=(BlurLockView)findViewById(R.id.blurLockView);
        blurLockView.setBlurredView(imageView);
        imageView=(ImageView)findViewById(R.id.backgroundImageView);
        blurLockView.setCorrectPassword("2222");
        blurLockView.setLeftButton("LEFT");
        blurLockView.setRightButton("RIGHT");
        blurLockView.setTypeface(Typeface.DEFAULT);
        blurLockView.setType(Password.NUMBER,false);

        blurLockView.setOnLeftButtonClickListener(new BlurLockView.OnLeftButtonClickListener() {
            @Override
            public void onClick() {
                Toast.makeText(MainActivity.this,"LEFT TAPPED",Toast.LENGTH_SHORT).show();
            }
        });

        blurLockView.setOnPasswordInputListener(new BlurLockView.OnPasswordInputListener() {
            @Override
            public void correct(String inputPassword) {

                Intent i= new Intent(getApplicationContext(),Main2ActivityAuth.class);

                Toast.makeText(MainActivity.this,"Pin is Correct",Toast.LENGTH_SHORT).show();
                blurLockView.hide(1000, HideType.FADE_OUT, EaseType.EaseInBounce);

                startActivity(i);


            }

            @Override
            public void incorrect(String inputPassword) {
                Toast.makeText(MainActivity.this,"Pin is Incorrect",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void input(String inputPassword) {

            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blurLockView.show(1000, ShowType.FADE_IN,EaseType.EaseInOutBack);
            }
        });
    }


}
