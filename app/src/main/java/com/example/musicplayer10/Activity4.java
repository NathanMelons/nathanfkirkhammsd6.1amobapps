package com.example.musicplayer10;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class Activity4 extends AppCompatActivity {//this activity is part of the music player that finds the tracks in your device

    Button btn_next,btn_previous,btn_pause;
    TextView songTextLabel;
    SeekBar songSeekbar;
    String sname;
    static MediaPlayer mymediaplayer;
    int position;
    ArrayList<File> mySongcoll;
    Thread updateSeekbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_4);

        btn_next=(Button)findViewById(R.id.skipnext);
        btn_pause=(Button)findViewById(R.id.pause);
        btn_previous=(Button)findViewById(R.id.skipprevious);
        songTextLabel=(TextView)findViewById(R.id.songLabel);
        songSeekbar=(SeekBar)findViewById(R.id.seekBar);

        getSupportActionBar().setTitle("Now Playing");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        updateSeekbar=new Thread(){

            @Override
            public void run()
            {
                int totalDuration=mymediaplayer.getDuration();
                int currentposition=0;
                while (currentposition<totalDuration)
                {
                    try
                    {
                        sleep(500);
                        currentposition=mymediaplayer.getCurrentPosition();
                        songSeekbar.setProgress(currentposition);

                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        };
        if(mymediaplayer!=null)
        {
            mymediaplayer.stop();
            mymediaplayer.release();
        }
        Intent i=getIntent();
         Bundle bundle=i.getExtras();

         mySongcoll=(ArrayList) bundle.getParcelableArrayList("songs");
        sname=mySongcoll.get(position).getName().toString();

        String songName=i.getStringExtra("songname");//songname from intent in activity 3

        songTextLabel.setText(songName);
        songTextLabel.setSelected(true);

        position=bundle.getInt("pos",0);



        Uri u=Uri.parse(mySongcoll.get(position).toString());

        mymediaplayer=MediaPlayer.create(getApplicationContext(),u);
        mymediaplayer.start();
        songSeekbar.setMax(mymediaplayer.getDuration());

        updateSeekbar.start();


        songSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                mymediaplayer.seekTo(seekBar.getProgress());
            }
        });

        btn_pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                songSeekbar.setMax(mymediaplayer.getDuration());
                if(mymediaplayer.isPlaying())
                {
                    btn_pause.setBackgroundResource(R.drawable.icon_play);
                    mymediaplayer.pause();
                }else
                    {
                        btn_pause.setBackgroundResource(R.drawable.icon_pause);
                        mymediaplayer.start();
                    }
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mymediaplayer.stop();
                mymediaplayer.release();
                position=((position+1)&mySongcoll.size());

                Uri u=Uri.parse(mySongcoll.get(position).toString());
                mymediaplayer=MediaPlayer.create(getApplicationContext(),u);

                sname=mySongcoll.get(position).getName().toString();
                songTextLabel.setText(sname);

                mymediaplayer.start();


                // additional references
                //https://www.youtube.com/watch?v=-ufjxQLwGM0&t=1501s
                //https://www.youtube.com/watch?v=Nw9JF55LDzE&t=84s
                //https://stackoverflow.com/questions/30978457/how-to-show-snackbar-when-activity-starts
                //https://developer.android.com/studio/run/emulator-console.html
                //https://stackoverflow.com/questions/2271131/display-the-current-time-and-date-in-an-android-application
                //https://developer.android.com/guide/topics/permissions/overview
                //https://stackoverflow.com/questions/23024831/android-shared-preferences-example
                //https://www.youtube.com/watch?v=Le47R9H3qow&feature=share



            }
        });
        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mymediaplayer.stop();
                mymediaplayer.release();

                position=((position-1)<0)?(mySongcoll.size()-1):(position-1);
                Uri u=Uri.parse(mySongcoll.get(position).toString());
                mymediaplayer=MediaPlayer.create(getApplicationContext(),u);

                sname=mySongcoll.get(position).getName().toString();
                songTextLabel.setText(sname);

                mymediaplayer.start();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {

        if(item.getItemId()==android.R.id.home)
        {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
