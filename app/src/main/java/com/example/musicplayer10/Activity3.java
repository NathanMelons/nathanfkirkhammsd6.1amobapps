package com.example.musicplayer10;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.share.Share;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Activity3 extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    ListView mySongList;
    String[] items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);

        Calendar calendar= Calendar.getInstance();
        final String currentdate=DateFormat.getDateInstance().format(calendar.getTime());
        showToast(currentdate);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, currentdate, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        mySongList=findViewById(R.id.mySongList);

        runtimePermissions();


    }
    public void runtimePermissions()
    {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        display();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.cancelPermissionRequest();
                    }
                }).check();

    }
    public ArrayList<File>findSong(File file)
    {
        ArrayList<File>arrayList=new ArrayList<>();
        File[] files=file.listFiles();
        for(File singleFile:files)
        {
            if(singleFile.isDirectory()&&!singleFile.isHidden())
            {
                arrayList.addAll(findSong(singleFile));
            }
            else
                {
                    if(singleFile.getName().endsWith(".mp3") )
                    {
                        arrayList.add(singleFile);
                    }
                }
        }
        return arrayList;
    }
    void display()
    {
        final ArrayList<File>mySongs=findSong(Environment.getExternalStorageDirectory());
        items=new String[mySongs.size()];
        for(int i=0; i<mySongs.size();i++)
        {
            items[i]=mySongs.get(i).getName().toString().replace("mp3","");
        }
        ArrayAdapter<String>myAdapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,items);
        mySongList.setAdapter(myAdapter);

        mySongList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String songName=mySongList.getItemAtPosition(position).toString();

                startActivity(new Intent(getApplicationContext(),Activity4.class)
                        .putExtra("songs",mySongs).putExtra("songname",songName)
                        .putExtra("pos",position));
            }
        });
    }

    private void showToast(String msg){
        Toast t = Toast.makeText(this, msg, Toast.LENGTH_SHORT);
        t.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences prefs=PreferenceManager.getDefaultSharedPreferences(this);
        int sound=prefs.getInt("in",0);
        View parentLayout=findViewById(android.R.id.content);
        Snackbar.make(parentLayout,"snackbar",Snackbar.LENGTH_LONG).setAction("close", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar= Calendar.getInstance();
                String currentdate=DateFormat.getDateInstance().format(calendar.getTime());
                showToast(currentdate);
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        Calendar calendar= Calendar.getInstance();
        String currentdate=DateFormat.getDateInstance().format(calendar.getTime());
        showToast(currentdate);
    }

    protected void onResume()
    {
        super.onResume();
        //Main2ActivityAuth active;
        View parentLayout=findViewById(android.R.id.content);
        Snackbar.make(parentLayout,"snackbar",Snackbar.LENGTH_LONG).setAction("close", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar= Calendar.getInstance();
                String currentdate=DateFormat.getDateInstance().format(calendar.getTime());
                showToast(currentdate);//show date in snackbar
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Calendar calendar= Calendar.getInstance();
        String currentdate=DateFormat.getDateInstance().format(calendar.getTime());
        showToast(currentdate);

        SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(Activity3.this);
        SharedPreferences.Editor editor=prefs.edit();
        editor.putInt("in",0);
        editor.apply();


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity3, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        startActivity(new Intent(getApplicationContext(),Activity5.class));//accesses class with api+recyclerview

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home)//pinlockppage
        {
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        } else if (id == R.id.nav_gallery)//authenticationpage
        {
            startActivity(new Intent(getApplicationContext(),Main2ActivityAuth.class));
        } else if (id == R.id.nav_slideshow) {


        } else if (id == R.id.nav_tools){//exit
            System.exit(0);

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
