package com.example.musicplayer10;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MusicHTTPSClient {
    private  String PRIME_URL="https://orion.apiseeds.com/api/music/lyric/";
    private  String API_KEY="?apikey=MhabkleQd4CIVIna31LAptuqizBC3BVYc8FKaoBBud2WZv4xZ2sm2n8oRQMwmfjU";

    public String getMusicData(String artist,String song)
    {
        HttpsURLConnection con=null;
        InputStream is=null;
        try
        {
            String url=PRIME_URL+artist+"/"+song;
            url+=API_KEY;
            con=(HttpsURLConnection)(new URL(url)).openConnection();
            con.setRequestMethod("GET");
            con.setDoInput(true);
            con.setDoOutput(true);//if file not found, change to false
            con.connect();

            StringBuffer buffer=new StringBuffer();
            is=con.getInputStream();
            BufferedReader br=new BufferedReader(new InputStreamReader(is));
            String line=null;
            while ((line=br.readLine())!=null)
                buffer.append(line+"\r\n");
            is.close();
            con.disconnect();
            return buffer.toString();
        }catch (Throwable t)
        {
            t.printStackTrace();
        }
        finally {
            try { is.close(); }catch (Throwable t){}
            try {con.disconnect(); }catch (Throwable t){}
        }
        return null;
    }

}
